# Gitpod Demos Workspace Image

This is a mirror of Gitpod's workspace-full and workspace-base images to a Gitlab hosted registry.

There is a gitlab ci job that does a pull and mirror to this repo, but the mirror.sh script can be run in a workspace as well.

Because of registry path differences, `gitpod/workspace-full` translates to `registry.gitlab.com/gitpod-demos/workspace-images/full` in gitlab (stored as `full` in the images associated with the workspace-images repo)

## Gitpod Workspace Full

`registry.gitlab.com/gitpod-demos/workspace-images/full:latest`

## Gitpod Workspace Base

`registry.gitlab.com/gitpod-demos/workspace-images/base:latest`

## Timestamped images

The job runs at 3am UTC time and adds an ISO 8601 formatted tag. Even if nothing else has changed in the image, there will always be a tagged image for your day.

This allows you to use `image: registry.gitlab.com/gitpod-demos/workspace-images/base:2023-07-26` in your .gitpod.yml if you want to manually update your workspace base images (changing 2023-07-26 to $TODAY).