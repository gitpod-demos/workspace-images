#!/bin/bash


TODAY=$(date -I)

echo $GITLAB_PAT | docker login -u $GITLAB_USER --password-stdin registry.gitlab.com

/usr/bin/docker pull gitpod/workspace-full:latest

/usr/bin/docker image tag gitpod/workspace-full:latest registry.gitlab.com/gitpod-demos/workspace-images/full:latest
/usr/bin/docker image tag gitpod/workspace-full:latest "registry.gitlab.com/gitpod-demos/workspace-images/full:${TODAY}"

/usr/bin/docker image push registry.gitlab.com/gitpod-demos/workspace-images/full:latest
/usr/bin/docker image push "registry.gitlab.com/gitpod-demos/workspace-images/full:${TODAY}"

/usr/bin/docker pull gitpod/workspace-python:latest

/usr/bin/docker image tag gitpod/workspace-python:latest registry.gitlab.com/gitpod-demos/workspace-images/python:latest
/usr/bin/docker image tag gitpod/workspace-python:latest "registry.gitlab.com/gitpod-demos/workspace-images/python:${TODAY}"

/usr/bin/docker image push registry.gitlab.com/gitpod-demos/workspace-images/python:latest
/usr/bin/docker image push "registry.gitlab.com/gitpod-demos/workspace-images/python:${TODAY}"

/usr/bin/docker pull gitpod/workspace-base:latest

/usr/bin/docker image tag gitpod/workspace-base:latest registry.gitlab.com/gitpod-demos/workspace-images/base:latest

/usr/bin/docker image tag gitpod/workspace-base:latest "registry.gitlab.com/gitpod-demos/workspace-images/base:${TODAY}"

/usr/bin/docker image push registry.gitlab.com/gitpod-demos/workspace-images/base:latest
/usr/bin/docker image push "registry.gitlab.com/gitpod-demos/workspace-images/base:${TODAY}"
